# 高德地图车机版(共存版)
基于高德地图车机版官方apk软件包制作，使用apktool反编译，仅供学习交流使用。

您不得（且您不得允许其他人）为任何商业目的使用该产品或服务，从而直接或间接获得任何形式的收益。

## 高德服务条款
[高德服务条款](https://cache.amap.com/h5/h5/publish/241/index.html)

### 生成签名文件
```
keytool -genkey -alias xxx.keystore -keyalg RSA -validity 3650 -keystore xxx.keystore
```
* -genkey：产生证书文件
* -alias：产生别名
* -keystore：指定密钥库的 xxx.keystore 文件
* -validity：证书的有效天数

### 进行签名
```
jarsigner -verbose -keystore xxx.keystore xxxxx.apk xxx.keystore
```

## 具体版本见分支信息



# 反编译步骤

### 1. 使用 apktool d xxx.apk 反编译apk文件
### 2. 修改 AndroidManifest.xml 文件中的以下内容
- package="com.autoavi.xxx"
- <permission android:name="com.autoavi.xxx
- <provider android:authorities="com.autoavi.xxx
### 3. 修改 strings.xml 文件中的 <string name="app_name">xxx</string>
### 4. 使用 apktool b 打包apk文件
### 5. 若出现应用打开异常，则使用 jadx 工具查找apk文件错误提示信息，找到判断代码，并将判断代码返回结果进行修改

#### 5.1 搜索弹窗提示信息，找到定义弹出对话框内容的代码

![image_jadx_1](README.assets/image_jadx_1.png)

#### 5.2 找到调用上一步弹出对话框的方法代码

![image_jadx_2](README.assets/image_jadx_2.png)

#### 5.3 从上一步中判断出 z80.d() 方法进行了判断，则 z80.d() 方法返回为true时则不会有弹窗信息

#### 5.4 找到 z80.d() 方法的代码，需要将代码修改为 return true，则需要修改z80.smali文件

![image_jadx_3](README.assets/image_jadx_3.png)

#### 5.5 修改 z80.smali

```
.xxxx xxxx
const/4 p0, 0x1
return p0
```



![image_smali_1](README.assets/image_smali_1.png)